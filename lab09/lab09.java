public class lab09{
  
  public static int[] copy(int[] a){//this method is to duplicate the array, and return a new one
    int[] a2=new int[a.length];
    for(int i=0; i<a.length; i++){
      a2[i]=a[i];
    }
    return a2;
  }
  
  public static void inverter(int[] a){//this method is to reverse the array  
    int j = a.length-1;
    for(int i=0; i<(a.length/2); i++){
      int num = a[i];
      a[i]=a[j];
      a[j]=num;  
      j--;
    } 
  }
  
  public static int[] inverter2(int[] a){//this method is the combination of the first two labs
   int[] copya=copy(a);
      for(int i=0; i<(copya.length/2); i++){
      int num = copya[i];
      copya[i]=copya[copya.length-1-i];
      copya[copya.length-1-i]=num;
  }
      return copya;
  }
  
  public static void print(int[] a){// it is to print out all elements in the array
    for(int i=0; i<a.length; i++){
    System.out.print(a[i]+" ");
  }
    System.out.println();
  }
  
  public static void main(String[] args){
    int[] array0={0,1,2,3,4,5,6,7};
    int[] array1=copy(array0);
    int[] array2=copy(array0);
    inverter(array0);
    print(array0);
    inverter2(array1);
    print(array1);
    int[] array3=inverter2(array2);
    print(array3);
  }
  
}