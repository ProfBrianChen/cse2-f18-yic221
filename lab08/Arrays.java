//Yiwei Cheng
//lab08
import java.util.Random;
public class Arrays{
  
  public static void main(String args[]){
  int[] array1=new int[100];//it contains 100 numbers
  int[] array2=new int[100];
  Random randomGenerator= new Random();
  int i=0;
  int j=0;
  System.out.print("Array 1 holds the following integers:");
  for(i=0;i<array1.length;i++){//assign random number to array1
    array1[i]=randomGenerator.nextInt(100);
    System.out.print(array1[i]+" ");//for check
  }
  System.out.println();
  for(i=0;i<array1.length;i++){
    j=array1[i];
    array2[j]++;
  }
  
  for(i=0;i<array2.length;i++){
    if(array2[i]>0){
    System.out.println(i+" occurs "+array2[i]+ " times");
    }   
  }
  
 
  }  //the end of main method 
}//the end of public class