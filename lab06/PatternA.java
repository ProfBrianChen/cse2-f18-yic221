import java.util.Scanner;//Scanner
public class PatternA{
  public static void main (String[] arg){
  Scanner myScanner=new Scanner(System.in);
  System.out.println(" please enter an integer between 1 and 10.");
  int num=0;
  int rows = 0;
  int rownum = 0;
  
  boolean answer=false;
    while(!answer){
      
    if (myScanner.hasNextInt()) {
       num=myScanner.nextInt(); 
       
       
        if (num<1||num>10){
           System.out.println("the number is out of range, so please enter an integer between 1 and 10 again.");
         }
        else{
        answer= true;
       // System.out.println("the answer is "+num); //for checking
        }
    }
    else {
      myScanner.next();
      System.out.println("it is not an integer, so please enter an integer 1 and 10 again.");
    }
    }
 
   
    for(rows = 1;rows<=num;rows++){
      for(rownum = 1; rownum <= rows; rownum++){
        System.out.print(rownum + " ");
      }
      System.out.println();
    }
   
   
   
   
   
   }
    
     
  }

   
