import java.util.Scanner;
public class PatternC{
public static void main (String[] arg){
  
  Scanner myScanner = new Scanner(System.in);

  System.out.println(" please enter an integer between 1 and 10.");
  int num=0;
  int rows = 0;
  int rownum = 0;
  String outcome= " ";
  
  boolean answer=false;
    while(!answer){
      
    if (myScanner.hasNextInt()) {
       num=myScanner.nextInt(); 
       
       
        if (num<1||num>10){
           System.out.println("the number is out of range, so please enter an integer between 1 and 10 again.");
         }
        else{
        answer= true;
       // System.out.println("the answer is "+num); //for checking
        }
    }
    else {
      myScanner.next();
      System.out.println("it is not an integer, so please enter an integer 1 and 10 again.");
    }
    }
  
  
  //System.out.println("The number you is: " + num);

      for(rownum = 1; rownum <= num; rownum++){
    outcome = rownum+outcome;
      System.out.printf("%11s\n",outcome);
}
}
}