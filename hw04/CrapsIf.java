//////////////
//Yiwei Cheng
//Sep.24 2018
//The purpose is to print out the slang terminology by rolling two dice.
import java.util.Scanner;
public class CrapsIf{
  // main method required for every Java program
 public static void main (String[] args) {
   Scanner myScanner=new Scanner(System.in);
   System.out.println("If you like randomly cast dice, please type 1. Or if you like to state the two dice you want to evaluate, please type 2.");
   int answer=myScanner.nextInt();
   
   int diceNum1;
   int diceNum2;
   //use if statements
   //input diceNum1 and diceNum2
   if (answer==1){
      diceNum1=(int)(Math.random()*(6))+1;
      diceNum2=(int)(Math.random()*(6))+1;
    System.out.println("The number of dice 1 is "+ diceNum1);
    System.out.println("The number of dice 2 is "+ diceNum2);
     //after having two dice number, there are mutiple outcomes
        if (diceNum1==1&&diceNum2==1){
     System.out.println("The name is Snake Eyes.");
       }
       else if ((diceNum1==1 && diceNum2==2)||(diceNum1==2 && diceNum2==1)){
     System.out.println("The name is Ace Deuce.");   
       }
       else if (diceNum1==2 && diceNum2==2){
     System.out.println("The name is Hard four.");
   }
       else if ((diceNum1==1 && diceNum2==3)||(diceNum1==3&&diceNum2==1)){
         System.out.println("The name is Easy Four.");
       }
       else if ((diceNum1==2 && diceNum2==3)||(diceNum1==3&&diceNum2==2)||(diceNum1==1&&diceNum2==4)||(diceNum1==4&&diceNum2==1)){
         System.out.println("The name is Fever five.");
 }
      else if ((diceNum1==3 && diceNum2==3)){
     System.out.println("The name is Hard Six.");   
       }
      else if ((diceNum1==2 && diceNum2==4)||(diceNum1==4&&diceNum2==2)||(diceNum1==1&&diceNum2==5)||(diceNum1==5&&diceNum2==1)){
         System.out.println("The name is Easy Six.");
      }
      else if ((diceNum1==3 && diceNum2==4)||(diceNum1==4&&diceNum2==3)||(diceNum1==1&&diceNum2==6)||(diceNum1==6&&diceNum2==1)||(diceNum1==2&&diceNum2==4)||(diceNum1==4&&diceNum2==2)){
         System.out.println("The name is Seven out.") ;
      }
        else if ((diceNum1==4 && diceNum2==4)){
     System.out.println("The name is Hard Eight.");   
       }
       else if ((diceNum1==5 && diceNum2==3)||(diceNum1==3&&diceNum2==5)||(diceNum1==2 && diceNum2==6)||(diceNum1==6&&diceNum2==2)){
         System.out.println("The name is Easy Eight.");
       }
       else if ((diceNum1==5 && diceNum2==4)||(diceNum1==4&&diceNum2==5)||(diceNum1==3 && diceNum2==6)||(diceNum1==6&&diceNum2==3)){
         System.out.println("The name is Nine.");
       }
       else if ((diceNum1==5 && diceNum2==5)){
     System.out.println("The name is Hard Ten.");   
       }
       else if ((diceNum1==4 && diceNum2==6)||(diceNum1==6 && diceNum2==4)){
     System.out.println("The name is Easy Ten.");   
       }
      else if ((diceNum1==5 && diceNum2==6)||(diceNum1==6 && diceNum2==5)){
     System.out.println("The name is Yo-leven.");   
       }
     else if ((diceNum1==6 && diceNum2==6)){
     System.out.println("The name is Boxcars.");  
  
     }
   }
   
   
   //if the users choose to assign dice numbers by theirselves
   else if (answer==2){
     System.out.println("Please type the first dice number you want to evaluate.");
      diceNum1=myScanner.nextInt();
     System.out.println("Please type the second dice number you want to evaluate.");
      diceNum2=myScanner.nextInt();
     System.out.println("The number of dice 1 is "+ diceNum1);
     System.out.println("The number of dice 2 is "+ diceNum2);
     
       if (diceNum1==1&&diceNum2==1){
     System.out.println("The name is Snake Eyes.");
       }
       else if ((diceNum1==1 && diceNum2==2)||(diceNum1==2 && diceNum2==1)){
     System.out.println("The name is Ace Deuce.");   
       }
       else if (diceNum1==2 && diceNum2==2){
     System.out.println("The name is Hard four.");
   }
       else if ((diceNum1==1 && diceNum2==3)||(diceNum1==3&&diceNum2==1)){
         System.out.println("The name is Easy Four.");
       }
       else if ((diceNum1==2 && diceNum2==3)||(diceNum1==3&&diceNum2==2)||(diceNum1==1&&diceNum2==4)||(diceNum1==4&&diceNum2==1)){
         System.out.println("The name is Fever five.");
 }
      else if ((diceNum1==3 && diceNum2==3)){
     System.out.println("The name is Hard Six.");   
       }
      else if ((diceNum1==2 && diceNum2==4)||(diceNum1==4&&diceNum2==2)||(diceNum1==1&&diceNum2==5)||(diceNum1==5&&diceNum2==1)){
         System.out.println("The name is Easy Six.");
      }
      else if ((diceNum1==3 && diceNum2==4)||(diceNum1==4&&diceNum2==3)||(diceNum1==1&&diceNum2==6)||(diceNum1==6&&diceNum2==1)||(diceNum1==2&&diceNum2==4)||(diceNum1==4&&diceNum2==2)){
         System.out.println("The name is Seven out.");   
      }
        else if ((diceNum1==4 && diceNum2==4)){
     System.out.println("The name is Hard Eight.");   
       }
       else if ((diceNum1==5 && diceNum2==3)||(diceNum1==3&&diceNum2==5)||(diceNum1==2 && diceNum2==6)||(diceNum1==6&&diceNum2==2)){
         System.out.println("The name is Easy Eight.");
       }
       else if ((diceNum1==5 && diceNum2==4)||(diceNum1==4&&diceNum2==5)||(diceNum1==3 && diceNum2==6)||(diceNum1==6&&diceNum2==3)){
         System.out.println("The name is Nine.");
       }
       else if ((diceNum1==5 && diceNum2==5)){
     System.out.println("The name is Hard Ten.");   
       }
       else if ((diceNum1==4 && diceNum2==6)||(diceNum1==6 && diceNum2==4)){
     System.out.println("The name is Easy Ten.");   
       }
      else if ((diceNum1==5 && diceNum2==6)||(diceNum1==6 && diceNum2==5)){
     System.out.println("The name is Yo-leven.");   
       }
     else if ((diceNum1==6 && diceNum2==6)){
     System.out.println("The name is Boxcars.");  
     }
      else if (diceNum1>6||diceNum2>6){
     System.out.println("Invaild information.");
      }
   }
                    
   
   
       
	}   // end of main method
} //end of class