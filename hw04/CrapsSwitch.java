/////////////
//Yiwei Cheng
//Sep.25 2018
//The purpose is to print out the slang terminology by rolling two dice.
import java.util.Scanner;
public class CrapsSwitch{
  // main method required for every Java program
 public static void main (String[] args) {
 Scanner myScanner=new Scanner(System.in);
 System.out.println("If you like randomly cast dice, please type 1. Or if you like to state the two dice you want to evaluate, please type 2.");
 int answer=myScanner.nextInt();
   
 switch(answer){
   case 1:
   int diceNum1=(int)(Math.random()*(6))+1;
   int diceNum2=(int)(Math.random()*(6))+1;
     //output the result for checking and clearfication
    System.out.println("The number of dice 1 is "+ diceNum1);
    System.out.println("The number of dice 2 is "+ diceNum2);
   
 int diceTotal=diceNum1+diceNum2;
 int diceDifference=diceNum1-diceNum2;
   switch(diceDifference){
     case 0://it means diceNum1=diceNum2
       
       switch(diceTotal){//their total/2 is the dice number for both 1 and 2
         case 2:
         System.out.println("The name is Snake Eyes.");
         break;
         case 4:
         System.out.println("The name is Hard Four.");
         break;
         case 6:
         System.out.println("The name is Hard Six.");
         break;
         case 8:
         System.out.println("The name is Hard Eight.");
         break;
         case 10:
         System.out.println("The name is Hard Ten.");
         break;
         case 12:
         System.out.println("The name is Boxcars.");   
       }
     break;
       
     default://it is for the situation where diceNum1 is equal to diceNum2
       switch(diceTotal){
         case 3:
         System.out.println("The name is Ace Deuce.");   
         break;
         case 4:
         System.out.println("The name is Easy Four.");  
         break;
         case 5:
         System.out.println("The name is Fever Five."); 
         break;
         case 6:
         System.out.println("The name is Easy Six."); 
         break;
         case 7:
         System.out.println("The name is Seven Out."); 
         break;
         case 8:
         System.out.println("The name is Easy Eight."); 
         break;
         case 9:
         System.out.println("The name is Nine."); 
         break;
         case 10:
         System.out.println("The name is Easy Ten.");
         break;
         case 11:
         System.out.println("The name is Yo-leven."); 
         break;
         default: 
         System.out.println("Invaild information.");
         break;
           
         
           
   }
     break;
   }
     break;
   
     case 2://almost the same code with assigned dice numbers
     System.out.println("Please type the first dice number you want to evaluate.");
     diceNum1=myScanner.nextInt();
     System.out.println("Please type the second dice number you want to evaluate.");
     diceNum2=myScanner.nextInt();
     System.out.println("The number of dice 1 is "+ diceNum1);
     System.out.println("The number of dice 2 is "+ diceNum2);
       
     diceTotal=diceNum1+diceNum2;
     diceDifference=diceNum1-diceNum2;
    
      switch(diceDifference){
      case 0:
       
       switch(diceTotal){
         case 2:
         System.out.println("The name is Snake Eyes.");
         break;
         case 4:
         System.out.println("The name is Hard Four.");
         break;
         case 6:
         System.out.println("The name is Hard Six.");
         break;
         case 8:
         System.out.println("The name is Hard Eight.");
         break;
         case 10:
         System.out.println("The name is Hard Ten.");
         break;
         case 12:
         System.out.println("The name is Boxcars.");   
           break;
       }
     break;
       
     default:
       switch(diceTotal){
         case 3:
         System.out.println("The name is Ace Deuce.");   
         break;
         case 4:
         System.out.println("The name is Easy Four.");  
         break;
         case 5:
         System.out.println("The name is Fever Five."); 
         break;
         case 6:
         System.out.println("The name is Easy Six."); 
         break;
         case 7:
         System.out.println("The name is Seven Out."); 
         break;
         case 8:
         System.out.println("The name is Easy Eight."); 
         break;
         case 9:
         System.out.println("The name is Nine."); 
         break;
         case 10:
         System.out.println("The name is Easy Ten.");
         break;
         case 11:
         System.out.println("The name is Yo-leven."); 
         break;
         default: 
         System.out.println("Invaild information.");
         break;
       }        
         break;    
      }

   
     break;
     //for checking and notation
     default: 
         System.out.println("Invaild information.");
         break;


}//the end of switch(answer)
   
 }
}
