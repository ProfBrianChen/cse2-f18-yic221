//Yiwei Cheng
//yic221
import java.util.Scanner;
import java.util.Random;
public class hw08{ 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); 
} 
System.out.println();
printArray(cards);//first method is to print the array out
shuffle(cards); 
printArray(cards); 
while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   printArray(hand);
   index = index - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
  } 

public static void printArray(String[] list){
  for (int i = 0; i < list.length; i++){  
    System.out.print(list[i]+" "); }
  System.out.println();
}

public static void shuffle(String[] list){
    Random randomGenerator = new Random(); 
        for (int i = 0; i < 52; i++){ 
        int random = randomGenerator.nextInt(list.length);//generate random numbers and switch the number with one number in the list
        String num = list[i];
        list[i]=list[random];
        list[random] = num;              
        } 
    }


public static String[] getHand(String[] lists, int index, int numCards){
   String[] hand = new String[numCards];
   String[] suitNames={"C","H","S","D"};    
   String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
   String[] cards = new String[52]; //generate cards by the same way
   if(numCards>index+1){
     for (int j = 0; j<52;j++){
       cards[j]=rankNames[j%13]+suitNames[j/13];
}
     shuffle(cards);
     index = 51;
   }
   for(int i = numCards -1; i>=0;i--){
     hand[i] = lists[index - 4 + i];
   }
   return hand;
 }




}
