////////////////
//Yiwei Cheng
//cse002-310
//9.17.2018
import java.util.Scanner;
//the purpose of this program is to prompt the user for 
//the dimensions of a pyramid and return the volume inside the pyramid
public class Pyramid{
    			// main method required for every Java program
   			public static void main(String[] args) {
          
          Scanner myScanner = new Scanner( System.in );
          
          System.out.print("The square side of the pyramid is (input length): ");
            double squaresidePyramid = myScanner.nextDouble();//input the square side of the pyramid
          
          System.out.print("The height of the pyramid is (input height): ");
            double heightPyramid = myScanner.nextDouble();//input the height of pyramid
          
          double volume=heightPyramid*squaresidePyramid*squaresidePyramid/3;//calculate the volume
          
            System.out.print("The volume inside the pyramid is: "+volume);
                    }  //end of main method   
  	} //end of class