////////////////
//Yiwei Cheng
//cse002-310
//9.17.2018

import java.util.Scanner;
//the purpose of this code is to ask the user for doubles that represent the number of acres of land affected 
//by hurricane precipitation and how many inches of rain were dropped on average
public class Convert{
    			// main method required for every Java program
   			public static void main(String[] args) {
          
          Scanner myScanner = new Scanner( System.in );
          
          System.out.print("Enter the affected area in acres: ");
            double affectedArea = myScanner.nextDouble();//input the affected area in acres
          
          System.out.print("Enter the rainfall in the affected area: ");
            double rainfall= myScanner.nextDouble();//input the rainfall in the affected area
          
          double rainQuantity=affectedArea*rainfall*9.08169e-13*27154;//We want to calculate rain quantity and then 
          //convert gallons to cubic miles.
          
            System.out.print(rainQuantity+" cubic miles");
          }  //end of main method   
  	} //end of class

        