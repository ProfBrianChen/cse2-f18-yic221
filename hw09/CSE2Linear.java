import java.util.Scanner;
import java.util.Random;
public class CSE2Linear{ 
public static void main(String[] args) { 
  
Scanner myScan= new Scanner(System.in);

System.out.println("Enter 15 ascending ints for final grades in CSE2: ");

int[] array=new int[15];//there are 15 numbers in this array

int num=0;
  
for(int i=0; i<15; i++){
boolean answer1= false;
boolean answer2= false;
boolean answer3= false;
  
//check everything
while(!answer1){
  answer1=myScan.hasNextInt();//check if it is an int
  if(answer1){
    array[i]=myScan.nextInt();
    //System.out.print(array[i]); for checking 
  }
  else{
    System.out.println("It is not an int. Enter again:");
  myScan.next();
  }
}

while(!answer2){
  if(array[i]>=0 && array[i]<=100){//check the range
    answer2= true;
  }
  else{
    System.out.println("It is out of range. Enter again:");
  array[i]=myScan.nextInt();
  }
}

if (i>0){//if it is the first number in the array, then there is no need to check it
  num=array[i-1];
}
//check if the number is greater than the last one
while(!answer3){
  if(array[i]>=num){
    answer3=true;
  }
  else{
    System.out.println("It is not greater than the last number you enterd. Enter again:");
  array[i]=myScan.nextInt();
  }
  
}
  
}//end of for loop
 
printarray(array);

System.out.println("Enter a grade to search for: ");

int grade =myScan.nextInt();// the first one is using binary search

binarysearch(array, grade);

scramble(array);

printarray(array);

System.out.println("Enter a grade to search for: ");//the second one is using linear search

int grade2 =myScan.nextInt();

linearsearch(array, grade2);

}

public static void printarray(int[] a){// it is to print out all numbers in the array
  for(int i=0; i<a.length;i++){
    System.out.print(a[i]+" ");}
  
}

public static void binarysearch(int[] a, int b){//binary search
  int mid = 0;
  int low = 0;
  int high = a.length - 1;
  int answer = -1;
  int counts = 0;//count how many iterations
  
  while(high >= low){
    mid = (low + high)/2;
    if(b<a[mid]){
      high = mid - 1;
      counts++;
    }
    else if(b>a[mid]){
      low = mid + 1;
      counts++;
    }
    else if(b==a[mid]){
      answer = mid;
      counts++;
    break;
  }
  }
  if(answer == -1){
    System.out.println(b + " was not found in the list with " + counts + " iterations.");
}
  else if(answer == mid){
    System.out.println(b + " was found in the list with " + counts + " iterations.");
  }
}


public static void scramble(int[] list){//scramble all numbers in this array
    Random randomGenerator = new Random(); 
        for (int i = 0; i <= 14; i++){ 
        int random = randomGenerator.nextInt(list.length);//generate random numbers and switch the number with one number in the list
        int num = list[i];
        list[i]=list[random];
        list[random] = num;              
        } 
    }
public static void linearsearch(int[] array, int b){//linear search is to compare every number in the array with the target number
  int count=0;
  int determinant = 0;
  for(int i=0;i<15;i++){
    count++;
    if(array[i]==b){
      System.out.println(b + " was  found in the list with " + count + " iterations.");
      determinant = 0;
      break;
    }
    else{determinant++;
      if(determinant==15){
         System.out.println(b + " was not found in the list with " + determinant + " iterations.");
      }
    }
    
    }
}
}