import java.util.Random;//import method Random
import java.util.Scanner;
public class RemoveElements{
  public static void main(String [] arg){
 Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
 String answer="";
 do{
   System.out.print("Random input 10 ints [0-9]");
   num = randomInput();
   String out = "The original array is:";
   out += listArray(num);
   System.out.println(out);
 
   System.out.print("Enter the index ");
   index = scan.nextInt();
   newArray1 = delete(num,index);
   String out1="The output array is ";
   out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
   System.out.println(out1);
 
      System.out.print("Enter the target value ");
   target = scan.nextInt();
   newArray2 = remove(num,target);
   String out2="The output array is ";
   out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
   System.out.println(out2);
    
   System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
   answer=scan.next();
 }while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
 String out="{";
 for(int j=0;j<num.length;j++){
   if(j>0){
     out+=", ";
   }
   out+=num[j];
 }
 out+="} ";
 return out;
  }
  
  public static int[] randomInput(){
    Random random=new Random();// assign random numbers to the array by for loop
    int array[]=new int[10];
    for(int i=0;i<10;i++){
      array[i]=random.nextInt(10);
    }
     return array; 
      
  }
  
  public static int[] delete(int[] list, int pos){
    boolean check=false;
    while(!check){//check if it is out of range
    if (pos<0||pos>9){
      Scanner scan=new Scanner(System.in);
      System.out.println("it is out of range, enter again:");
        pos=scan.nextInt();
    }
    else{check=true;}// it is in the range then go to the next step
    }
    int array[]= new int[9];//deleting one number means there are 9 numbers left
    for(int i=0;i<pos;i++){
      array[i]=list[i];
    }
    
    for(int j=pos;j<9;j++){
      array[j]=list[j+1];
  
    }
  return array; 
  }
  
  public static int[] remove(int[] list,int target){
    int count=0;//firstly figure out what the size of the array is
    int j=0;
    for(int i=0;i<=9;i++){
      if(list[i]==target){
      count++;
      }
      
    }
    int array[]= new int[10-count];
    for (int i=0;i<=9;i++){// and put everything else in the new array
      if (list[i]!=target){
        array[j]=list[i];
        j++;
      }
    
 
    }
    return array;
}
}
