//Yiwei Cheng
import java.util.Scanner;
public class hw07{
  public static void main(String args[]){//this code is to use the menu and edit the text
    //main method
    Scanner myScan=new Scanner(System.in);
    String text;
    text=text();
    System.out.println("You entered: " + text);
    
    char letter=' ';
    
    String findWord=" ";

    while(letter != 'q'){ //letter is ' ' so it will print out the menu first
    letter = printMenu(); 
    
    if(letter == 'c'){ 
      System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(text)); 
    } 
    
    else if(letter == 'w'){ 
      System.out.println("Number of words: " + getNumOfWords(text)); 
    } 
    
    else if(letter == 'f'){ 
      System.out.println("Enter a word or phrase to be found:"); 
      findWord = myScan.nextLine(); 
      System.out.println("\"" + findWord + "\" instances: " + findText(findWord,text)); 
    } 
    
    else if(letter == 'r'){ 
      System.out.println("Edited text: " + replaceExclamation(text)); 
    } 
    
    else if(letter =='s'){ 
      System.out.println("Edited text: " + shortenSpace(text)); 
    } 
  } 
  } 
  
  
  public static String text(){ // this method is to print out the text
    Scanner myScan=new Scanner(System.in);
    System.out.println("Enter a sample text: ");
    String a = myScan.nextLine();
    return a;
  }
  
  public static char printMenu(){//method of print menu  is to print out the menu
    char letter = ' '; 
    Scanner scan = new Scanner(System.in); 
     
    System.out.println("Menu"); 
    System.out.println("c - Number of non-whitespace characters"); 
    System.out.println("w - Number of words"); 
    System.out.println("f - Find text"); 
    System.out.println("r - Replace all !'s"); 
    System.out.println("s - Shorten spaces"); 
    System.out.println("q - Quit"); 
    System.out.println(); 
    System.out.println("Choose an option:"); 
    letter = scan.next().charAt(0);
    while(letter != 'c' && letter != 'w' && letter != 'f' && letter != 'r' && letter != 's' && letter != 'q'){ 
    System.out.println("Choose an option again: "); //check if it is a vaild letter
    letter = scan.next().charAt(0); 
    }
    return letter;
  }
  
    
  
  public static String getNumOfNonWSCharacters(String a){
  //the purpose is to find out how many characters in the String
    String num="";
    int characters=0;
    for(int i = 0; i < a.length(); i++){  
     if(a.charAt(i) != ' ' ){ //if it is a space, it means there is a word
      characters++; 
    } 
    } 
    num = String.valueOf(characters);
    return num;
  }
  
  public static String getNumOfWords(String a){
    //this is to count how many words in the String
    String numResult = ""; 
    int numWord = 0; 
    int k = 0; 
    String line = shortenSpace(a); 
    for(k = 0; k< line.length(); k++){ 
      if(line.charAt(k) == ' '){ 
        numWord++; 
      } 
    } 
    numResult = String.valueOf(numWord + 1); 
    return numResult; 
  } 
    
    
    
   
  public static String findText(String a, String b){//it is to find there are how many same words in the sentence
    String num = ""; 
    int numSameWord = 0; 
    int i = 0; 
    int j = 0; 
    for(i = 0; i < b.length(); i++){ 
      if(b.charAt(i) == a.charAt(j)){ 
        j++; 
        if(j==a.length()){ 
        numSameWord++; 
        j = 0; 
        } 
      } 
          } 
    //convert result from char to string 
    num = String.valueOf(numSameWord); 
    return num; 
  } 
   
  public static String replaceExclamation(String a){//this method is to replace"!"
  return a.replaceAll("!","."); 
  } 
   
  
    public static String shortenSpace(String a){//this method is to replace the double space by the single space 
    String space = " "; 
    String doubleSpace = "  "; 
    return a.replaceAll(doubleSpace,space); 
  } 


  
}
    