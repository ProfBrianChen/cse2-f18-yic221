////////////////
//Yiwei Cheng
//Oct 4 2018
import java.util.Scanner;//import method scanner
public class CourseInfo{
  public static void main(String[] arg){  
    
    Scanner myScanner= new Scanner(System.in);
    
    boolean answer= false;
    
    System.out.println("please enter the course number.");
    while(!answer){//use infinite loop to ask the question again when the type is wrong
       answer= myScanner.hasNextInt();//courseNum should be an int
    if (answer){
      int courseNum=myScanner.nextInt();
    System.out.println("The course number is " + courseNum);  
    }
  
   else{  
     System.out.println("please enter the course number.");
     myScanner.next();  
   }
   }//end of while(courseNum)
    
   answer= false;
     System.out.println("please enter the department name.");//department name should be a string
    while(!answer){
       answer= myScanner.hasNext();
    if (answer){
      String departName=myScanner.next();
    System.out.println("The department name is " + departName);  
    }
  
   else{  
     System.out.println("please enter the department name.");
     myScanner.next();  
   }
   }//end of while(department)
    
    
   answer= false;
     System.out.println("please enter the number of hours it meets in a week.");
     while(!answer){
       answer=myScanner.hasNextDouble();//the hours could be a double or an int
    if(answer){
      Double timeSpend=myScanner.nextDouble();
     System.out.println("The number of hours it meets in a week is "+ timeSpend);
    }
     else{
        System.out.println("please enter the number of hours it meets in a week.");
        myScanner.next();
     }
     }
       
    
       
   answer=false;
    System.out.println("please enter the time when the class starts, like XX(hour)XX(minute)");
    while(!answer){
      answer=myScanner.hasNextInt();//the given formate is an int in order to check more easily
     if(answer){
       int timeStart=myScanner.nextInt();
       System.out.println("The time when the class starts is " + timeStart);
     }
      else{
       System.out.println("please enter the time when the class starts, like XX(hour)XX(minute)");
         myScanner.next();
      } 
    }
       
       
    answer= false;
     System.out.println("please enter the instructor name.");
    while(!answer){
       answer= myScanner.hasNext();//instructorName should be a string
    if (answer){
      String instructorName=myScanner.next();
    System.out.println("The instructor name is " + instructorName);  
    }
  
   else{  
     System.out.println("please enter the instructor name.");
     myScanner.next();  
   }
      
   }
       
       
     answer=false;
    System.out.println("please enter the number of students");
    while(!answer){
      answer=myScanner.hasNextInt();//number of students should be an int
     if(answer){
       int studentNum=myScanner.nextInt();
       System.out.println("The number of students is " + studentNum);
     }
      else{
       System.out.println("please enter the number of students");
         myScanner.next();
      } 
    }
           
    
       
    
  }//end of public static 
  
}//end of the public class
  
