//////////////
//Yiwei Cheng
//Oct.20 2018
//The purpose is to pick up a random card from the deck.
public class CardGenerator {
  	// main method required for every Java program
   	public static void main(String[] args) {
//use if statements
      //{"Clubs", "Hearts", "Spades", "Diamonds"}
      //{"Ace", "2","3","4","5","6","7","8","9","10","Jack","Queen","King"}
      //input suits and number
      int cardNum=(int)(Math.random()*(52))+1;
      String suits="";
      String number = "";
      //seperate numbers into 4 groups
      if(cardNum >= 1 && cardNum <= 13){
        suits = "Diamonds";
      }
      else if (cardNum >=14 && cardNum<=26){
        suits = "Clubs";
      }
      
      else if(cardNum>=27 && cardNum<=39){
        suits="Hearts";
      }
      else if(cardNum>=40 && cardNum<=52){
        suits="Spades";
      }
      //limit the range of cardNum from 52 to 13
      cardNum=cardNum%13;
      
      switch(cardNum){//switch cardNum to the number on the card
        case 1: number= "Ace";
          break;
        case 2: number= "2";
          break;
        case 3: number= "3";
          break;
        case 4: number= "4";
          break;
        case 5: number= "5";
          break;
        case 6: number= "6";
          break;
        case 7: number= "7";
          break;
        case 8: number= "8";
          break;
        case 9: number= "9";
          break;
        case 10: number= "10";
          break;
        case 11: number= "Jack";
          break;
        case 12: number= "Queen";
          break;   
        case 0: number= "King";
          break;
        default: number="invaild message";
          break;
      }
      //output the result
        System.out.print("You picked the " +number+ " of " + suits);
      
  
      
	}   // end of main method
} //end of class

