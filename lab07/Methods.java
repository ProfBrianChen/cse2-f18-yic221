//Yiwei Cheng
//lab07
import java.util.Random;
import java.util.Scanner;
public class Methods{
  public static void main(String[] arg){//the purpose is to generate a sentence
    Random randomGenerator = new Random();
    Scanner myscanner = new Scanner(System.in);
    int determinant = 1;//let 1 be the determinant and "No" in order to be easier
    while(determinant != 2){
    System.out.println("The " + adjective(randomGenerator.nextInt(10)) + " " + noun1(randomGenerator.nextInt(10)) + " " + verbPast(randomGenerator.nextInt(10)) + " the " + adjective(randomGenerator.nextInt(10)) + " " + noun2(randomGenerator.nextInt(10)) +"." );
    System.out.println("Do you satisfy this sentence? Enter 1 or 2");
    System.out.println("1.No 2.Yes");
    determinant = myscanner.nextInt();    
    
  }
    int num=randomGenerator.nextInt(10);//we need noun as a subject so we need to make sure it is a random number but not change in the next sentences
    sentence1(num);
    sentence2(num);
  }
    public static String adjective(int a){

      String adj="";
      switch (a){//seperate cases
        case 0: adj="polite";
        break;
        case 1: adj="quick";
        break;
        case 2: adj="lazy";
        break;
        case 3: adj="beautiful";
        break;
        case 4: adj="brave";
        break;
        case 5: adj="aggressive";
        break;
        case 6: adj="kind";
        break;
        case 7: adj="large";
        break;
        case 8: adj="nervous";
        break;
        case 9: adj="strange";
        break;
        default: System.out.println("error");
        
      }
      return adj;
    }
    public static String noun1(int a){
      String n1="";
      switch (a){
        case 0: n1="rabbit";
        break;
        case 1: n1="cat";
        break;
        case 2: n1="lion";
        break;
        case 3: n1="fox";
        break;
        case 4: n1="zebra";
        break;
        case 5: n1="horse";
        break;
        case 6: n1="tiger";
        break;
        case 7: n1="snake";
        break;
        case 8: n1="pig";
        break;
        case 9: n1="duck";
        break;
        default: System.out.println("error");
      }
      return n1;
    }
    
    
    public static String noun2(int a){
      
      String n2 =" ";
      switch(a){
        case 0:n2="chicken";
        break;
        case 1:n2="dinosaur";
        break;
        case 2:n2="falcon";
        break;
        case 3:n2="dove";
        break;
        case 4: n2="hawk";
        break;
        case 5: n2="horse";
        break;
        case 6: n2="rat";
        break;
        case 7: n2="deer";
        break;
        case 8: n2="Turkey";
        break;
        case 9: n2="bull";
        break;
      }
      return n2;
    }
      
      public static String verbPast(int a){
        
      String verb="";
      switch(a){
        case 0: verb= "jumped";
        break;
        case 1: verb= "hit";
        break;
        case 2: verb= "loved";
        break;
        case 3: verb= "kissed";
        break;
        case 4: verb= "spoke to";
        break;
        case 5: verb= "laughed at";
        break;
        case 6: verb= "flew over";
        break;
        case 7: verb= "lived with";
        break;
        case 8: verb= "answered";
        break;
        case 9: verb= "ate";
        break;
      }
        return verb;
        
}
      public static void sentence1(int a){
        Random randomGenerator = new Random();
        System.out.println("The " + adjective(randomGenerator.nextInt(10)) + " " + noun1(a) + " " + verbPast(randomGenerator.nextInt(10)) + " the " + adjective(randomGenerator.nextInt(10)) + " " + noun2(randomGenerator.nextInt(10)) +"." ); 
      }
     
      public static void sentence2(int a){
        Random randomGenerator = new Random();
        System.out.println("This " + noun1(a) + " was " + adjective(randomGenerator.nextInt(10)) + " to " + adjective(randomGenerator.nextInt(10)) + " " + noun2(randomGenerator.nextInt(10)) +"." );
        System.out.println("The " + noun1(a) + " said that " + "the " + noun2(randomGenerator.nextInt(10)) +" is " +adjective(randomGenerator.nextInt(10)) + ".");
        System.out.println("That " + noun1(a) + " " + verbPast(randomGenerator.nextInt(10)) + " the " + noun2(randomGenerator.nextInt(10)) + "!");
      }
    
   }
  

