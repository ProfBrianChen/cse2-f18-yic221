//Yiwei Cheng
//October 23
import java.util.Scanner;
public class EncryptedX{//the purpose of the code is to print an X with *
  public static void main(String arg[]){
  Scanner myScanner=new Scanner(System.in);
    
  System.out.println(" please enter an integer between 0 and 100.");
  int num=0;
  
  int rows = 0;
  int rownum = 0;
  
  boolean answer=false;
  
    while(!answer){
      
    if (myScanner.hasNextInt()) {//check if it is an integer
       num=myScanner.nextInt(); 
       
       
        if (num<0||num>100){//check the range
           System.out.println("the number is out of range, so please enter an integer between 0 and 100 again.");
         }//ask again
        else{
        answer= true;
       // System.out.println("the answer is "+num); //for checking
        }
    }
    else {
      myScanner.next();
      System.out.println("it is not an integer, so please enter an integer 0 and 100 again.");//ask again
    }
    }
    
    
    for(rows=1;rows<=num+1;rows++){//it controls how many rows it will have
      for(rownum=1;rownum<=num+1;rownum++){//it controls what outcome is
        if(rownum==rows){System.out.print(" ");}
        else if(rownum==num+2-rows){System.out.print(" ");}
        else{System.out.print("*");}
      
      } 
      System.out.println();
    }
      
  
  
  
  
  }
}
 