//////////////
//Yiwei Cheng
//Sep. 10 2018
//cse002-310
//yic221
public class Arithmetic {
  // main method required for every Java program
   	public static void main(String[] args) {
      
      
//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;
      

//cost of pants
double costPants=numPants*pantsPrice;
  
 //cost of sweatshirts
  double costShirts=numShirts*shirtPrice;
  
  //cost of belts
  double costBelts=numBelts*beltCost;
      
   //Output of Cost
   System.out.println("The cost of pants is "+costPants);
   System.out.println("The cost of shirts is "+costShirts);
   System.out.println("The cost of belts is "+costBelts);
     

   //Tax of Pants
   double taxPants=(int)(costPants*paSalesTax*100)/100.0;
     //Tax of Shirts
        double taxShirts=(int)(costShirts*paSalesTax*100)/100.0;
     //Tax of Belts   
     double taxBelts=(int)(costBelts*paSalesTax*100)/100.0;
     
     //Output of tax
    System.out.println("The tax of pants is "+taxPants);
     System.out.println("The tax of shirts is "+taxShirts);
     System.out.println("The tax of belts is "+taxBelts);
     
       //total cost(before tax)
  double totalCost=costPants+costShirts+costBelts;
    //Output of totalCost
    System.out.println("The total cost is "+totalCost);
      
      //total tax
      double totalTax=(int)(100*taxBelts+100*taxShirts+100*taxPants)/100.0;
      // Output of total tax
      System.out.println("The total tax is "+totalTax);
      
      //the total cost of the purchases including tax
      double totalPaid=totalTax+totalCost;
      
      // display the output of the total cost of the purchases including tax
      System.out.println("The total paid for this transaction including sales tax is "+totalPaid);
      
      
      
      
            
	}   // end of main method
} //end of class
      
      
      
      
      
     
     
     
   
  
  