//Yiwei Cheng
//hw05
//10/9/2018
import java.util.Scanner;//Scanner
import java.text.DecimalFormat;//for decimal places
public class Hw05{
  public static void main (String[] arg){
  Scanner myScanner=new Scanner(System.in);
  //we will calculate a double by using onePair, twoPairs, same3 and same4, so they need to be double
    
  //initialize everything
  double onePair=0;
  double twoPairs=0;
  double same3=0;
  double same4=0;
  
  int numCard1=0;
  int numCard2=0;
  int numCard3=0;
  int numCard4=0;
  int numCard5=0;
  
  int num1=0;
  int num2=0;
  int num3=0;
  int num4=0;
  int num5=0;
  int num6=0;
  int num7=0;
  int num8=0;
  int num9=0;
  int num10=0;
  int num11=0;
  int num12=0;
  int num13=0;     

  double prob4=0;
  double prob3=0;
  double prob2=0;
  double prob1=0;
  //Let user put the number in.
  System.out.println("please enter how many times you want it to generate hands.");
  
  double numHands= myScanner.nextInt();
  
  DecimalFormat df3  = new DecimalFormat("##.000");
  
    //for while loop
  for(int i=0; i<numHands;i++){
  numCard1=(int)(Math.random()*(52))+1;//assign random number between 1 to 52
  
  numCard2=(int)(Math.random()*(52))+1;
  
  while(numCard2==numCard1){ //if the number is the same number then assign another one
  numCard2=(int)(Math.random()*(52))+1;     
  }
  
  numCard3=(int)(Math.random()*(52))+1;
  
  while(numCard3==numCard1||numCard2==numCard3){ 
  numCard3=(int)(Math.random()*(52))+1;     
  }
    
  numCard4=(int)(Math.random()*(52))+1;
  
  while(numCard4==numCard1||numCard3==numCard4||numCard2==numCard4){ 
  numCard4=(int)(Math.random()*(52))+1;     
  }
  
  numCard5=(int)(Math.random()*(52))+1;
  
  while(numCard5==numCard1||numCard3==numCard5||numCard2==numCard5||numCard4==numCard5){ 
  numCard5=(int)(Math.random()*(52))+1;     
  }

  //calculate the number on each card
  
  numCard1=numCard1%13;
  
  //System.out.println("Card1 is " + numCard1);
  
  numCard2=numCard2%13;
  
  //System.out.println("Card2 is " + numCard2);
    
  numCard3=numCard3%13;
  
  //System.out.println("Card3 is " + numCard3);
    
  numCard4=numCard4%13;
  
 // System.out.println("Card4 is " + numCard4);
    
  numCard5=numCard5%13;
  
 // System.out.println("Card5 is " + numCard5);

  num1=0;//calculate how many same numbers are in one hand
  if(numCard1==1){
  num1++;
  }
   if(numCard2==1){
  num1++;
  }
   if(numCard3==1){
  num1++;
  }
   if(numCard4==1){
  num1++;
  }
   if(numCard5==1){
  num1++;
  }
  //2 
   num2=0;
   if(numCard1==2){
  num2++;
  }
   if(numCard2==2){
  num2++;
  }
   if(numCard3==2){
  num2++;
  }
   if(numCard4==2){
  num2++;
  }
   if(numCard5==2){
  num2++;
  }
   
   //3
   num3=0;
   if(numCard1==3){ 
  num3++; 
  } 
   if(numCard2==3){ 
  num3++; 
  } 
   if(numCard3==3){ 
  num3++; 
  } 
   if(numCard4==3){ 
  num3++; 
  } 
   if(numCard5==3){ 
  num3++; 
  } 
//4
   num4=0;
   if(numCard1==4){ 
  num4++; 
  } 
   if(numCard2==4){ 
  num4++; 
  } 
   if(numCard3==4){ 
  num4++; 
  } 
   if(numCard4==4){ 
  num4++; 
  } 
   if(numCard5==4){ 
  num4++; 
  } 
//5
   num5=0;
   if(numCard1==5){ 
  num5++; 
  } 
   if(numCard2==5){ 
  num5++; 
  } 
   if(numCard3==5){ 
  num5++; 
  } 
   if(numCard4==5){ 
  num5++; 
  } 
   if(numCard5==5){ 
  num5++; 
  } 
   //6
   num6=0;
   if(numCard1==6){ 
  num5++; 
  } 
   if(numCard2==6){ 
  num5++; 
  } 
   if(numCard3==6){ 
  num5++; 
  } 
   if(numCard4==6){ 
  num5++; 
  } 
   if(numCard5==6){ 
  num5++; 
  } 
   //7
   num7=0;
   if(numCard1==7){ 
  num7++; 
  } 
   if(numCard2==7){ 
  num7++; 
  } 
   if(numCard3==7){ 
  num7++; 
  } 
   if(numCard4==7){ 
  num7++; 
  } 
   if(numCard5==7){ 
  num7++; 
  } 
   //8
   num8=0;
   
   if(numCard1==8){ 
  num8++; 
  } 
   if(numCard2==8){ 
  num8++; 
  } 
   if(numCard3==8){ 
  num8++; 
  } 
   if(numCard4==8){ 
  num8++; 
  } 
   if(numCard5==8){ 
  num8++; 
  } 
   //9
   num9=0;
   if(numCard1==9){ 
  num9++; 
  } 
   if(numCard2==9){ 
  num9++; 
  } 
   if(numCard3==9){ 
  num9++; 
  } 
   if(numCard4==9){ 
  num9++; 
  } 
   if(numCard5==9){ 
  num9++; 
  } 
   //10
   num10=0;
   if(numCard1==10){ 
  num10++; 
  } 
   if(numCard2==10){ 
  num10++; 
  } 
   if(numCard3==10){ 
  num10++; 
  } 
   if(numCard4==10){ 
  num10++; 
  } 
   if(numCard5==10){ 
  num10++; 
  } 
//11
   num11=0;
      if(numCard1==11){ 
  num11++; 
  } 
   if(numCard2==11){ 
  num11++; 
  } 
   if(numCard3==11){ 
  num11++; 
  } 
   if(numCard4==11){ 
  num11++; 
  } 
   if(numCard5==11){ 
  num11++; 
  } 
//12
   num12=0;
  if(numCard1==12){ 
  num12++; 
  } 
   if(numCard2==12){ 
  num12++; 
  } 
   if(numCard3==12){ 
  num12++; 
  } 
   if(numCard4==12){ 
  num12++; 
  } 
   if(numCard5==12){ 
  num12++; 
  } 
//13
   num13=0;
   if(numCard1==13){ 
  num13++; 
  } 
   if(numCard2==13){ 
  num13++; 
  } 
   if(numCard3==13){ 
  num13++; 
  } 
   if(numCard4==13){ 
  num13++; 
  } 
   if(numCard5==13){ 
  num13++; 
  } 

   //calculate how many pairs are in one hand
   if (num1==2||num2==2||num3==2||num4==2||num5==2||num6==2||num7==2||num8==2||num9==2||num10==2||num11==2||
       num12==2||num13==2){
   onePair++;
   }
    //calculate how many three of a kind is in one hand
    if (num1==3||num2==3||num3==3||num4==3||num5==3||num6==3||num7==3||num8==3||num9==3||num10==3||num11==3||
       num12==3||num13==3){
   same3++;
   }
    //calculate how many four of a kind is in one hand 
    if (num1==4||num2==4||num3==4||num4==4||num5==4||num6==4||num7==4||num8==4||num9==4||num10==4||num11==4||
       num12==4||num13==4){
   same4++;
   }
    
    //calculate how many 2 pairs is in one hand
       if(num1==2&&num2==2||num1==2&&num3==2||num1==2&&num4==2||num1==2&&num5==2||num1==2&&num6==2||num1==2&&num7==2||
          num1==2&&num8==2||num1==2&&num9==2||num1==2&&num10==2||num1==2&&num11==2||num1==2&&num12==2||num1==2&&num13==2||
          num2==2&&num3==2||num2==2&&num4==2||num2==2&&num5==2||num2==2&&num6==2||num2==2&&num7==2||num2==2&&num8==2||
          num2==2&&num9==2||num2==2&&num10==2||num2==2&&num11==2||num2==2&&num12==2||num2==2&&num13==2||num3==2&&num4==2||
          num3==2&&num5==2||num3==2&&num6==2||num3==2&&num7==2||num3==2&&num8==2||num3==2&&num9==2||num3==2&&num10==2||
          num3==2&&num11==2||num3==2&&num12==2||num3==2&&num13==2||num4==2&&num5==2||num4==2&&num6==2||num4==2&&num7==2||
          num4==2&&num8==2||num4==2&&num9==2||num4==2&&num10==2||num4==2&&num11==2||num4==2&&num12==2||num4==2&&num13==2||
          num5==2&&num6==2||num5==2&&num7==2||num5==2&&num8==2||num5==2&&num9==2||num5==2&&num10==2||num5==2&&num11==2||
          num5==2&&num12==2||num5==2&&num13==2||num6==2&&num7==2||num7==2&&num8==2||num7==2&&num9==2||num7==2&&num10==2||
          num7==2&&num11==2||num7==2&&num12==2||num7==2&&num13==2||num7==2&&num8==2||num7==2&&num9==2||num7==2&&num10==2||
          num7==2&&num11==2||num7==2&&num12==2||num7==2&&num13==2||num8==2&&num9==2||num8==2&&num10==2||num8==2&&num11==2||
          num8==2&&num12==2||num8==2&&num13==2||num9==2&&num10==2||num9==2&&num11==2||num9==2&&num12==2||num9==2&&num13==2||
          num10==2&&num11==2&&num10==2&&num12==2||num10==2&&num13==2||num11==2&&num12==2||num11==2&&num13==2||num12==2&&num13==2){ 
          twoPairs++;  
   }
        
      }//end of the whole while loop
     prob4=same4/numHands;
     prob3=same3/numHands;
     prob2=twoPairs/numHands;
     prob1=onePair/numHands;
   //print out with 3 decimal places  

  System.out.println("The number of loops: " + (int)numHands);
  System.out.println("The probability of Four-of-a-kind: "+ df3.format(prob4));
  System.out.println("The probability of Three-of-a-kind: " + df3.format(prob3)); 
  System.out.println("The probability of Two-pair: " + df3.format(prob2)); 
  System.out.println("The probability of One-pair: " + df3.format(prob1)); 

  
  }
}
 
