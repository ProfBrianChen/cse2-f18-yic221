//////////////
//Yiwei Cheng
//yic221
//cse002-310
//9.3.2018
public class WelcomeClass{
  public static void main (String args[]){
    System.out.println("    -----------");
    System.out.println("    | WELCOME |");
    System.out.println("    -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-Y--I--C--2--2--1->");
    System.out.println(" \\/ \\/ \\/ \\/ \\/ \\/");
    System.out.println(" v  v  v  v  v  v");
  }
}