//////////////
//Yiwei Cheng
//Sep. 6 2018
//
public class Cyclometer {
  	// main method required for every Java program
   	public static void main(String[] args) {

		int secsTrip1=480;  // input a variable secsTrip1, and assign the variable a value
    int secsTrip2=3220;  //input a variable secsTrip2, and assign the variable a value
		int countsTrip1=1561;  //input a variable countsTrip1, and assign the variable a value
		int countsTrip2=9037; //input a variable countsTrip2, and assign the variable a value
    
    double wheelDiameter=27.0,  //input a variable "wheelDiameter" and assign it a value
  	PI=3.14159, //input another variable "PI" and assign it a value
  	feetPerMile=5280,  //input another variable "feetPerMile" and assign it a value
  	inchesPerFoot=12,   //input another variable "inchesPerFoot" and assign it a value
  	secondsPerMinute=60;  //input another variable "secondsPerMinute" and assign it a value
  	double distanceTrip1, distanceTrip2,totalDistance;  
      //input other variables including "distanceTrip1", "distanceTrip2", "totalDistance"
  
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+
                       " minutes and had "+ countsTrip1+" counts.");
	  System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+
                       " minutes and had "+ countsTrip2+" counts.");
      
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	  totalDistance=distanceTrip1+distanceTrip2;
//Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
  	System.out.println("Trip 2 was "+distanceTrip2+" miles");
  	System.out.println("The total distance was "+totalDistance+" miles");



      
	}   // end of main method
} //end of class
